﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Wcfjson
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "EliminarPersona/{Id}", ResponseFormat = WebMessageFormat.Json)]
        string EliminarPersona(string Id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate="MostrarPersonas", ResponseFormat=WebMessageFormat.Json)]
        List<Persona> MostarPersonas();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MostrarPersona/{Id}", ResponseFormat = WebMessageFormat.Json)]
        Persona MostarPersona(string Id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MostrarAutos", ResponseFormat = WebMessageFormat.Json)]
        List<Auto> MostrarAutos();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MostrarAuto/{Id}", ResponseFormat = WebMessageFormat.Json)]
        Auto MostrarAuto (string id);

    }
}