﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Wcfjson
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        private ERjsonContainer DB = new ERjsonContainer();
        public List<Persona> MostarPersonas()
        {
            return DB.PersonaSet.ToList();
        }


        public Persona MostarPersona(string Id)
        {
            int aux = Convert.ToInt16(Id);
            return DB.PersonaSet.Where(p => p.Id == aux).First();
        }



        public string EliminarPersona(string Id)
        {
            int a = Convert.ToInt16(Id);
            if (DB.PersonaSet.Where(p => p.Id == a).First() == null)
            {
                return "no";
            }
            else return "si";
        }


        public List<Auto> MostrarAutos()
        {
            return DB.AutoSet.ToList();
        }

        public Auto MostrarAuto(string xx)
        {
            int aux = Convert.ToInt16(xx);
            return DB.AutoSet.Where(p => p.Id == aux).First();
            
        }
    }
}
